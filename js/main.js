"use strict"

//1. Cоздать объект student, который содержит следующие свойства: имя, фамилию, пол (isMale), контактные данные, методы: вывод адреса, смена пола (на противоположный).

// const student = {
//     name: 'Vasya',
//     surname: 'Pupkin',
//     isMale: true,
//     street: 'Lenina',
//     house: 43,
//     phoneNumber: +380931231741,
//     address: function() {
//         return `${this.street} ${this.house}`
//     },
//     changeMale: function(){
//         return this.isMale = !this.isMale;
//     }
// }

// student.changeMale();
// console.log(student);


// 2. Cоздать объект, который содержит свойства о факультете и кафедре, методы: переименование факультета (метод должен принимать в качестве параметра новое название факультета).

// const myObject = {
//     faculty: 'Super faculty for real guys',
//     department: 'Cat fans',
//     facultyChange: function(name){
//         return this.faculty = name;
//     }
// }

// myObject.facultyChange('Super faculty for real girls');
// console.log(myObject);

// 3 Создать функции-конструкторы:
// - книга (автор, название, год издания, издательство)
//   * предусмотреть в прототипе метод, который будет возвращать возраст книги в годах.

function Book(author, bookName, year, publishingHouse ) {
    if (!new.target) { 
        return new Book(author, bookName, year, publishingHouse);
    }

    this.author = author;
    this.bookName = bookName;
    this.year = year;
    this.publishingHouse = publishingHouse;

    this.bookAge = function() {
        const currentTime = new Date()
        return currentTime.getFullYear() - this.year;
    };
}

const myBook = new Book('Valodzi', 'Koni v chistom pole', 1996, 'Mestniy podval');

console.log(myBook.bookAge());

